/**
 * Created by Web-1 on 07.09.2015.
 */
$(document).ready(function() {                            // по завершению загрузки страницы
    $.getJSON( "json/content.json", function(obj) {
        $("#title").append("<h2>"+obj.title+"</h2>");
        $("#content").append("<p>"+obj.content+"</p>");
    });

    $.getJSON( "json/categories.json", function(obj) {
        $.each(obj, function(key, value) {
            $(".dropdown_inside").append("<li><a href='#' name="+value.parent+" id="+value.id+">"+value.name+"</a></li>");
        });
    });

    $(".dropdown_top").click(function(event) {
        $(".comment").empty()	//удалит содержимое всех элементов с классом comment
        var parent = event.target.name;
        var id = event.target.id;
        $.getJSON( "json/comments.json", function(obj) {
            $.each(obj, function(key, value) {
                if(id==value.id){

                        $(".comment").append("<p><span class='name'>"+value.name+"</span><br />"+value.content+"<p>")

                };
            });
        });
    });
});